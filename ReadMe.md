# Space gradient atmosphere libraries

Данный репозиторий создан для более быстрого подключения библиотек Космического градиента к проекту для атмосферной версии конструктора MiniSat

## Состав библиотек

```
Mux
Stm32f1_i2c
Sg_delay
Lsm303
Bmp280
```


## Пути для подключения библиотек
	

Данная библиотека с submodules должна быть добавлена в папку проекта `Core`

Для того, чтобы проект видел добавленные submodules, необходимо скопировать следующие строки в соответствующую область кода файла `main.c` или `main.h`, которая начинается со строк `/* USER CODE BEGIN Includes */` и заканчивается `/* USER CODE END Includes */`

```
#include "../sg_atmosphere_libs/stm32f1_i2c/sg_stm32f1_i2c.h"
#include "../sg_atmosphere_libs/sg_delay/SG_delay.h"
#include "../sg_atmosphere_libs/bmp280/sg_bmp280.h"
#include "../sg_atmosphere_libs/mux/sg_mux.h"
#include "../sg_atmosphere_libs/lsm303/SG_LSM303DLH.h"
```

## Пути для проекта

В самом проекте также необходимо прописать пути для того, чтобы компилятор видел эти файлы и компилировал их. Для этого нужно зайти в `настройки проекта -> C/C++ Build -> Settings -> MCU GCC Compiler -> Include Path`. Там для всех используемых библиотек необходимо добавить пути следующего вида:

```
../Core/sg_atmosphere_libs/stm32f1_i2c
../Core/sg_atmosphere_libs/sg_delay
../Core/sg_atmosphere_libs/bmp280
../Core/sg_atmosphere_libs/mux
../Core/sg_atmosphere_libs/lsm303
```

## sg_config.h

Для того, чтобы библиотеки не выдавали ошибки при компиляции, некоторые из них требуют наличия в проекте файла `sg_config.h`. В нем задаются настройки библиотек. Если они вам не требуются, то просто оставьте файл пустым.